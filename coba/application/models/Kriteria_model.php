<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kriteria_model extends CI_Model {

	protected $table = 'kriteria';

	 
	function tambah_data($data)
	{
		# code...
		$this->db->insert('kriteria',$data);
		return TRUE;
	}

	function ubah_data($data,$id_kriteria)
	{
		# code...
		$this->db->where('id_kriteria', $id_kriteria);
		$this->db->update('kriteria', $data);
		
		return TRUE;
	}

	 function baca_kriteria($id_kriteria)
	{
		# code...
		$query = $this->db->get_where('kriteria',array(
			'id_kriteria' => $id_kriteria
		));
		return $query->row();
	}

	function data_kriteria()
	{
		# code...
		$query = $this->db->get('kriteria');
		return $query->result();
	}
	function hapus($where) {
		$this->db->where($where); //yg adda didalam where saja yg akan terhapus
		$this->db->delete($this->table);
	}
}
