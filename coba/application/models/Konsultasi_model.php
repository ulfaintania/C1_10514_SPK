<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konsultasi_model extends CI_Model {

	protected $table = 'konsultasi';

	 
	function tambah_data($data)
	{
		# code...
		$this->db->insert('konsultasi',$data);
		return TRUE;
	}


	 function baca_konsultasi($id_konsultasi)
	{
		# code...
		$query = $this->db->get_where('konsultasi',array(
			'id_konsultasi' => $id_konsultasi
		));
		return $query->row();
	}

	function data_konsultasi()
	{
		# code...
		$query = $this->db->get('konsultasi');
		return $query->result();
	}
	
}
