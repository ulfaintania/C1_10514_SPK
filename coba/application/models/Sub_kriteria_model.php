<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sub_kriteria_model extends CI_Model {

	protected $table = 'sub_kriteria';

	 
	function tambah_data($data)
	{
		# code...
		$this->db->insert('sub_kriteria',$data);
		return TRUE;
	}

	function ubah_data($data,$id_sub_kriteria)
	{
		# code...
		$this->db->where('id_sub_kriteria', $id_sub_kriteria);
		$this->db->update('sub_kriteria', $data);
		
		return TRUE;
	}

	 function baca_sub_kriteria($id_sub_kriteria)
	{
		# code...
		$this->db->select('*');
		$this->db->from('kriteria,sub_kriteria');
		$this->db->where('kriteria.id_kriteria=sub_kriteria.id_kriteria');
		$this->db->where('sub_kriteria.id_sub_kriteria='.$id_sub_kriteria);

		$query = $this->db->get();
			
		return $query->result();

		/*$query = $this->db->get_where('sub_kriteria',array(
			'id_sub_kriteria' => $id_sub_kriteria
		));
		return $query->row();*/
	}

	function data_sub_kriteria()
	{
		# code...
		//JOIN
		/*$this->db->select('*');
		$this->db->select('sub_kriteria.id_sub_kriteria as id_sk');
		$this->db->select('kriteria.id_kriteria as id_k');
		$this->db->select('kriteria.kriteria as nama_k');
		$this->db->join('kriteria','kriteria.id_kriteria = sub_kriteria.id_kriteria');

		$query = $this->db->get('sub_kriteria');
		return $query->result();*/

		$this->db->select('*');
		$this->db->from('kriteria,sub_kriteria');
		$this->db->where('kriteria.id_kriteria=sub_kriteria.id_kriteria');

		$query = $this->db->get();
			
		return $query->result();
	}
	function hapus($where) {
		$this->db->where($where); //yg adda didalam where saja yg akan terhapus
		$this->db->delete($this->table);
	}
}
