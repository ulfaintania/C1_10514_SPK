<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alternatif_model extends CI_Model {

	protected $table = 'alternatif';

	 
	function tambah_data($data)
	{
		# code...
		$this->db->insert('alternatif',$data);
		return TRUE;
	}

	function ubah_data($data,$id)
	{
		# code...
		$this->db->where('id', $id);
		$this->db->update('alternatif', $data);
		
		return TRUE;
	}

	 function baca_alternatif($id)
	{
		# code...
		$query = $this->db->get_where('alternatif',array(
			'id' => $id
		));
		return $query->row();
	}

	function data_alternatif()
	{
		# code...
		$query = $this->db->get('alternatif');
		return $query->result();
	}
	function hapus($where) {
		$this->db->where($where); //yg adda didalam where saja yg akan terhapus
		$this->db->delete($this->table);
	}
}
