<div id="page-wrapper">
    <!-- isi kontentnya -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $title ?></h1>
        </div>
        <!-- /.col-lg-12 -->
         <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-3"> <a class="btn btn-primary" href="<?php echo base_url(); ?>alternatif/tambah"> Tambah alternatif</a></div>
            <div class="col-lg-3"></div>
            <div class="col-lg-3"></div>
            <div class="col-lg-3">
            <div class="input-group custom-search-form" >
                <input type="text" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button class="btn btn-default" type="button">
                     <i class="fa fa-search"></i>
                 </button>
                </span>
             </div>
             </div>
        </div>
         </div>
    </div>

    <br>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>alternatif</th>
                        <th>deskripsi</th>
                        <th>gambar</th>
                        <th>aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($alternatif as $item): ?>
                        <tr>
                            <td>
                                <?php echo $item->id; ?>
                            </td>
                            <td>
                                <?php echo $item->alternatif; ?>
                            </td>
                            <td>
                                <?php echo $item->deskripsi; ?>
                            </td>
                            <td>
                                <?php echo $item->gambar; ?>
                            </td>
                            <td>
                                <a class="btn btn-warning" href="<?php echo base_url(); ?>alternatif/ubah/<?php echo $item->id; ?>">Ubah</a>
                                <a class="btn btn-danger" href="<?php echo base_url(); ?>alternatif/delete/<?php echo $item->id; ?>">Delete</a>

                                
                               
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

</div>
        <!-- /#page-wrapper -->