<div id="page-wrapper">
    <!-- isi kontentnya -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $title ?></h1>
        </div>
        <!-- /.col-lg-12 -->
        <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-3"><a class="btn btn-primary" href="<?php echo base_url(); ?>Konsultasi/tambah"> Konsultasi </a></div>
            <div class="col-lg-3"></div>
            <div class="col-lg-3"></div>
            <div class="col-lg-3">
            <div class="input-group custom-search-form" >
                <input type="text" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button class="btn btn-default" type="button">
                     <i class="fa fa-search"></i>
                 </button>
                </span>
             </div>
             </div>
        </div>
         </div>
    </div>

    <br>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>Waktu Konsultasi</th>
                        <th>aksi</th>
                    </tr>
                </thead>
                
                <tbody>
                    <?php foreach ($konsultasi as $item): ?>
                        <tr>
                            <td>
                                <?php echo $item->id_konsultasi; ?>
                            </td>
                            <td>
                                <?php echo $item->nama; ?>
                            </td>
                           
                            <td>
                                <?php echo $item->waktu; ?>
                            </td>
                            <td>
                                <a href="" class="btn btn-sm btn-default"><i class="fa fa-file-text-o"></i> Detail</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

</div>
        <!-- /#page-wrapper -->