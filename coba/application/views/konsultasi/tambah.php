<div id="page-wrapper">
            <!-- isi kontentnya -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $title ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
           <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" action="<?php echo base_url(); ?>konsultasi/simpan" method="POST">
                                        <div class="form-group">
                                              <label>Silakan masukkan nama Anda</label>
                                              <input type="text" class="form-control" name="nama" required>
                                        </div>
                                        <div class="alert alert-info" style="margin-top: 30px">Tentukan preferensi Anda terhadap masing-masing kriteria berikut</a></div>
                                        
                                        
                                        
                                        <button type="submit" class="btn btn-info" style="margin-top: 15px"><i class="fa fa-check"></i> Proses</button>
                                    </form>
                                </div>
                                
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->