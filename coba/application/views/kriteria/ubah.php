<div id="page-wrapper">
            <!-- isi kontentnya -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $title ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
           <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" action="<?php echo base_url(); ?>kriteria/update" method="POST">
                                        <input style="display: none;" type="text" name="id_kriteria" value="<?php echo $kriteria->id_kriteria ?>">
                                        <div class="form-group">
                                            <label>Kriteria</label>
                                            <input name="kriteria" class="form-control" value="<?php echo $kriteria->kriteria ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Tipe</label>
                                            <select name="tipe" class="form-control" required>
                                                <!--<option value="<?php echo $kriteria->tipe ?>">Keuntungan</option>
                                                <option value="<?php echo $kriteria->tipe ?>">Biaya</option> -->
                                                <option value="Keuntungan">Keuntungan</option>
                                                <option value="Biaya">Biaya</option>
                                            </select>
                                        </div>
                                    
                                        <button type="submit" class="btn btn-default">Simpan</button>
                                        <button type="reset" class="btn btn-default">Bersihkan</button>
                                    </form>
                                </div>
                                
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->