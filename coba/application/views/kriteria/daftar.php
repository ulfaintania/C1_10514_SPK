<div id="page-wrapper">
    <!-- isi kontentnya -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $title ?></h1>
        </div>
        <!-- /.col-lg-12 -->
        <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-3"><a class="btn btn-primary" href="<?php echo base_url(); ?>kriteria/tambah"> Tambah Data Baru</a></div>
            <div class="col-lg-3"></div>
            <div class="col-lg-3"></div>
            <div class="col-lg-3">
            <div class="input-group custom-search-form" >
                <input type="text" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button class="btn btn-default" type="button">
                     <i class="fa fa-search"></i>
                 </button>
                </span>
             </div>
             </div>
        </div>
         </div>
    </div>

    <br>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>id_kriteria</th>
                        <th>kriteria</th>
                        <th>Tipe</th>
                        <th>aksi</th>
                    </tr>
                </thead>
                
                <tbody>
                    <?php foreach ($kriteria as $item): ?>
                        <tr>
                            <td>
                                <?php echo $item->id_kriteria; ?>
                            </td>
                            <td>
                                <?php echo $item->kriteria; ?>
                            </td>
                           
                            <td>
                                <?php echo $item->tipe; ?>
                            </td>
                            <td>
                                <a class="btn btn-warning" href="<?php echo base_url(); ?>kriteria/ubah/<?php echo $item->id_kriteria; ?>">Ubah</a>
                                <a class="btn btn-danger" href="<?php echo base_url(); ?>kriteria/delete/<?php echo $item->id_kriteria; ?>">Delete</a>

                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

</div>
        <!-- /#page-wrapper -->