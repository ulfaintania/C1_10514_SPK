<div id="page-wrapper">
            <!-- isi kontentnya -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $title ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
           <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" action="<?php echo base_url(); ?>sub_kriteria/update" method="POST">
                                        <?php foreach ($sub_kriteria as $item): ?>
                                            <input style="display: none;" type="text" name="id_sub_kriteria" value="<?php echo $item->id_sub_kriteria ?>">
                                            <div class="form-group">
                                                <label>Kriteria</label>
                                                <input name="id_kriteria" class="form-control" value="<?php echo $item->kriteria ?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Sub Kriteria</label>
                                                <input name="sub_kriteria" class="form-control" value="<?php echo $item->sub_kriteria ?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Nilai</label>
                                                <input type="number" min="1" max="100" name="nilai" class="form-control" value="<?php echo $item->nilai ?>">
                                            </div>
                                        <?php endforeach; ?>
                                    
                                        <button type="submit" class="btn btn-default">Simpan</button>
                                        <button type="reset" class="btn btn-default">Bersihkan</button>
                                    </form>
                                </div>
                                
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->