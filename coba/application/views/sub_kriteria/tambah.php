<div id="page-wrapper">
            <!-- isi kontentnya -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $title ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
           <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" action="<?php echo base_url(); ?>sub_kriteria/simpan" method="POST">
                                        <div class="form-group">
                                            <label>Kriteria</label>
                                            <select name="id_kriteria" class="form-control">
                                                <?php foreach ($kriteria as $item): ?>
                                                    <option value="<?php echo $item->id_kriteria; ?>"> <?php echo $item->kriteria ?></option>
                                                <?php endforeach; ?>
                                                                                                
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Sub Kriteria</label>
                                            <input name="sub_kriteria" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Nilai</label>
                                           
                                                <input type="number" id_kriteria="nilai" min="1" max="100" name="nilai" class="form-control">
                                        </div>
                                                                                
                                        <button type="submit" class="btn btn-default">Simpan</button>
                                        <button type="reset" class="btn btn-default">Bersihkan</button>
                                    </form>
                                </div>
                                
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->