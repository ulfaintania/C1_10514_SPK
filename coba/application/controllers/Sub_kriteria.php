<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sub_kriteria extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('sub_kriteria_model');
        $this->load->model('kriteria_model');
    }

	public function index()
	{

		$data['title'] = 'Sub Kriteria';
		$data['sub_kriteria'] = $this->sub_kriteria_model->data_sub_kriteria();

		$this->load->view('template/header');
		$this->load->view('sub_kriteria/daftar',$data);
		$this->load->view('template/footer');
	}
	public function tambah()
	{
		$data['title'] = 'Tambah Data Baru';
		$data['kriteria'] = $this->kriteria_model->data_kriteria();

		$this->load->view('template/header');
		
		$this->load->view('sub_kriteria/tambah',$data);
		$this->load->view('template/footer');
	}
	public function simpan()
	{

		$id_kriteria = $_POST['id_kriteria'];
		$sub_kriteria = $_POST['sub_kriteria'];
		$nilai = $_POST['nilai'];
		
		$data = array(
			'id_kriteria' => $id_kriteria,
			'sub_kriteria' => $sub_kriteria,
			'nilai' => $nilai
		);

		$this->sub_kriteria_model->tambah_data($data);

		redirect('sub_kriteria');
	}
	public function ubah($id_sub_kriteria)
	{
		$data['title'] = 'Ubah Sub Kriteria';
		$where = array(
			'id_sub_kriteria' => $id_sub_kriteria
		);
		$data['sub_kriteria'] = $this->sub_kriteria_model->baca_sub_kriteria($id_sub_kriteria);

		$this->load->view('template/header');	
		$this->load->view('sub_kriteria/ubah',$data);
		$this->load->view('template/footer');
	}
	public function update()
	{
		$id_sub_kriteria = $_POST['id_sub_kriteria'];
		$sub_kriteria = $_POST['sub_kriteria'];
		$nilai = $_POST['nilai'];

		$data = array(
			'sub_kriteria' => $sub_kriteria,
			'nilai' => $nilai
		);

		$this->sub_kriteria_model->ubah_data($data,$id_sub_kriteria);

		redirect('sub_kriteria');
	}
	public function delete($id_sub_kriteria) {
		//mana yg mau dihapus
		$where = array(
			'id_sub_kriteria' => $id_sub_kriteria
		);
		//proses hapus
		$this->sub_kriteria_model->hapus($where);
		//kembali ke halaman depan
		redirect('sub_kriteria');
	}
}
