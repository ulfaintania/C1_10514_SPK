<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kriteria extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('kriteria_model');
    }

	public function index()
	{

		$data['title'] = 'Kriteria';
		$data['kriteria'] = $this->kriteria_model->data_kriteria();

		$this->load->view('template/header');
		$this->load->view('kriteria/daftar',$data);
		$this->load->view('template/footer');
	}
	public function tambah()
	{
		$data['title'] = 'Tambah kriteria';

		$this->load->view('template/header');
		
		$this->load->view('kriteria/tambah',$data);
		$this->load->view('template/footer');
	}
	public function delete($id_kriteria) 
	{
		//mana yg mau dihapus
		$where = array(
			'id_kriteria' => $id_kriteria
		);
		//proses hapus
		$this->kriteria_model->hapus($where);
		//kembali ke halaman depan
		redirect('kriteria');
	}
	public function ubah($id_kriteria)
	{
		$data['title'] = 'Ubah kriteria';
		$where = array(
			'id_kriteria' => $id_kriteria
		);
		$data['kriteria'] = $this->kriteria_model->baca_kriteria($id_kriteria);

		$this->load->view('template/header');	
		$this->load->view('kriteria/ubah',$data);
		$this->load->view('template/footer');
	}
	public function simpan()
	{
		$kriteria = $_POST['kriteria'];
		$tipe = $_POST['tipe'];
		
		$data = array(
			'kriteria' => $kriteria,
			'tipe' => $tipe,
		);

		$this->kriteria_model->tambah_data($data);

		redirect('kriteria');
	}

	public function update()
	{
		$id_kriteria = $_POST['id_kriteria'];
		$kriteria = $_POST['kriteria'];
		$tipe = $_POST['tipe'];
		
		$data = array(
			'kriteria' => $kriteria,
			'tipe' => $tipe
		);

		$this->kriteria_model->ubah_data($data,$id_kriteria);

		redirect('kriteria');
	}
}
