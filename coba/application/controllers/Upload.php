<?php

class Upload extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->helper(array('form', 'url'));
                $this->load->library('upload');
        }

        public function index()
        {
                $this->load->view('upload_form', array('error' => ' ' ));
        }

        public function do_upload()
        {
                if($this->input->post('upload')) {
                $config = array(
                    'allowed_types' => 'jpg|jpeg|gif|png',
                    'upload_path' => $this->gallery_path,
                    'max_size' => 2000,
                    'file_name' => url_title($this->input->post('gambar'))
                );
                $this->load->library('upload', $config);
                $this->upload->do_upload();
                $file   = $this->upload->file_name;
        }

}
?>