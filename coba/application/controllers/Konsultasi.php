<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konsultasi extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('konsultasi_model');
        $this->load->model('konsultasi_model');
    }

	public function index()
	{

		$data['title'] = 'Riwayat Konsultasi';
		$data['konsultasi'] = $this->konsultasi_model->data_konsultasi();

		$this->load->view('template/header');
		$this->load->view('konsultasi/daftar',$data);
		$this->load->view('template/footer');
	}
	public function tambah()
	{
		$data['title'] = 'Tambah konsultasi';

		$this->load->view('template/header');
		$this->load->view('konsultasi/tambah',$data);
		$this->load->view('template/footer');
	}
	
	public function simpan()
	{
		$nama = $_POST['nama'];
		$waktu = $_POST['waktu'];

		$data = array(
			'nama' => $nama,
			'waktu' => date("Y-m-d H:i:s")
		);

		$this->konsultasi_model->tambah_data($data);

		redirect('konsultasi');
	}

	
}
