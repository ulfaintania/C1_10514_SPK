<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keluar extends CI_Controller {
	
	public function logout()
	{
		$this->session->sess_destroy();
		//redirect('user','refresh');
		redirect(base_url('user/login'));
	}
}
