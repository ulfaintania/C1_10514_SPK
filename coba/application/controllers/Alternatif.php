<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alternatif extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('alternatif_model');
    }

	public function index()
	{

		$data['title'] = 'Daftar alternatif';
		$data['alternatif'] = $this->alternatif_model->data_alternatif();

		$this->load->view('template/header');
		
		$this->load->view('alternatif/daftar',$data);
		$this->load->view('template/footer');
	}
	public function tambah()
	{
		$data['title'] = 'Tambah alternatif';

		$this->load->view('template/header');
		
		$this->load->view('alternatif/tambah',$data);
		$this->load->view('template/footer');
	}
	public function delete($id) {
		//mana yg mau dihapus
		$where = array(
			'id' => $id
		);
		//proses hapus
		$this->alternatif_model->hapus($where);
		//kembali ke halaman depan
		redirect('alternatif');
	}
	public function ubah($id)
	{
		$data['title'] = 'Ubah alternatif';
		$where = array(
			'id' => $id
		);
		$data['alternatif'] = $this->alternatif_model->baca_alternatif($id);

		$this->load->view('template/header');	
		$this->load->view('alternatif/ubah',$data);
		$this->load->view('template/footer');
	}
	public function simpan()
	{
		# code...
		$alternatif = $_POST['alternatif'];
		$deskripsi = $_POST['deskripsi'];
		$gambar = $_POST['gambar'];

		$data = array(
			'alternatif' => $alternatif,
			'deskripsi' => $deskripsi,
			'gambar' => $gambar
		);

		$this->alternatif_model->tambah_data($data);

		redirect('alternatif');
	}

	public function update()
	{
		# code...
		$id = $_POST['id'];
		$alternatif = $_POST['alternatif'];
		$deskripsi = $_POST['deskripsi'];
		$gambar = $_POST['gambar'];

		$data = array(
			'alternatif' => $alternatif,
			'deskripsi' => $deskripsi,
			'gambar' => $gambar
		);

		$this->alternatif_model->ubah_data($data,$id);

		redirect('alternatif');
	}
}
