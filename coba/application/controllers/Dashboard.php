<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct(){
		parent::__construct();
		
		//cek apakah user sudah login atau belum
		if($this->session->userdata('status') != 'login'){
			redirect(base_url('user/login'));
		}
	}


	public function index()
	{
		//$data['alternatif'] = $this->Alternatif
		$this->load->view('template/header');
		$this->load->view('dashboard/dashboard');
		$this->load->view('template/footer');
	}
}
