<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('m_user');
	
		
	}
	
	public function login()
	{
		$this->load->view('user/user');
	}
	public function verify()
	{
		//ambil data dari form login
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		//konstruksi query where
		$where = array(
			'username' => $username,
			'password' => sha1($password),
		);
		
		// cek ke database 
		$cek = $this->m_user->cek_login($where)->num_rows();
		
		//jika pasangan username dan password ditemukan
		if ($cek==1){
			//buat sesi baru
			$data_session = array (
				'username' => $username,
				'status' => 'login'
			);
			$this->session->set_userdata($data_session);
			
			redirect('dashboard');
		}
		
		//jika tidak ditemukan 
		else{
			$this->session->set_flashdata('error', 'Username atau password belum ada');
			//redirect(base_url('index.php/user/login'));
			$this->load->view('user/user');
		}
	}
	
	
}
